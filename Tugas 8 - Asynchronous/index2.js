var readBooksPromise = require('./promise.js');

var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]


const getPromise = (i, times) => {
    i >= books.length -0 ? '' :
    readBooksPromise(times, books[i], times => {
        return index + getPromise(i + 1, times)
    }).then((yes) => {
        console.log(yes);
        getPromise(i + 1, yes);
    }).catch((err) => {
        console.log(err);
    })
}

getPromise(0, 10000);

// Output:
// saya mulai membaca LOTR
// saya sudah selesai membaca LOTR, sisa waktu saya 7000
// 7000
// saya mulai membaca Fidas
// saya sudah selesai membaca Fidas, sisa waktu saya 5000
// 5000
// saya mulai membaca Kalkulus
// saya sudah selesai membaca Kalkulus, sisa waktu saya 1000
// 1000
