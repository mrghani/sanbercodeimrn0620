var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

const getCallback = (i, times) => {
    i >= books.length -0 ? '' :
    readBooks(times, books[i], times => {
        return i + getCallback(i + 1, times)
    })
}

getCallback(0, 10000);

// Output :
// saya membaca LOTR
// saya sudah membaca LOTR, sisa waktu saya 7000
// saya membaca Fidas
// saya sudah membaca Fidas, sisa waktu saya 5000
// saya membaca Kalkulus
// saya sudah membaca Kalkulus, sisa waktu saya 1000