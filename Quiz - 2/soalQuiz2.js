// 1. SOAL CLASS SCORE

class Score {
    constructor(subject, points, email) {
        this.subject = subject
        this.points = points
        this.email = email
    }
    average() {
        let _points = this.points;
        let sum = 0;
        _points.forEach(x => {
            sum += x;
        });

        let avg =  sum / _points.length;
        return avg;
    }
}

// TEST CASE
var _points = [10,20,30];
var test = new Score('iniSubject', _points, 'email@gmail.com');

console.log(test.average());


// --------------------------------------------------------------------------
// 2. SOAL Create Score

const data = [
    ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
]

function viewScores(data, subject) {
  let _email = data[0];
  let _val = [];


  for (let i = 1; i < data.length; i++) {
      let _obj = {
          email: `${data[i][0]}`,
          subject: subject,
          points: `${data[i][1]}`
      }

      _val.push(_obj);
  }
  return _val;
}

// TEST CASE
viewScores(data, "quiz-1");
viewScores(data, "quiz-2");
viewScores(data, "quiz-3");


// --------------------------------------------------------------------------
// 3. SOAL Recap Score

const data = [
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
]

function recapScores(data) {
    let arr = [];
    for (let i = 0; i < data.length; i++) {
        let _data = data[i];

        let _email = _data[0];
        let _avg = ((_data[1] + _data[2] + _data[3]) / 3).toFixed(1);
        let _predikat = '';

        if (_avg > 90) {
            _predikat = "honour";
        } else if (_avg > 80 && _avg <= 90){
            _predikat = "graduate";
        } else if (_avg > 70 && _avg <= 80){
            _predikat = "participant";
        } else {
            _predikat = "Anak yang punya yayasan";
        }

        let person = {
            'Email': _email,
            'Rata-rata': _avg,
            'Predikat': _predikat
        }
        arr.push(person);
    }
    return arr;
}

recapScores(data);