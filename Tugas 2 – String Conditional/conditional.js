var nama =  'John';
var peran = '';

// IF ELSE
if (nama != ""){
    if (peran != ""){
        if (peran == "Penyihir") {
            console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
            console.log(`Halo ${peran} ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`);
        } else if (peran == "Guard"){
            console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
            console.log(`Halo ${peran} ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.`);
        } else { // Werewolf
            console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
            console.log(`Halo ${peran} ${nama}, Kamu akan memakan mangsa setiap malam!`);
        }
    } else {
        console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`);
    }
} else {
    console.log("Nama harus diisi!");
}


// -------------------------------------------------------------
// SWITCH CASE
var hari = 21; 
var bulan = 1; 
var tahun = 1945;

// Flag
var fhari = true, fbulan = true, ftahun = true;

// Result
var rhari, rbulan, rtahun;

switch(true){
    case (hari >= 1 && hari <= 31):
        rhari = hari;
        break;
    default:
        fhari = false;
        rhari = `${hari} tidak terdaftar sebagai tanggal`;
        break;
}


switch(bulan){
    case 1:
        rbulan = "Januari"
        break;
    case 2:
        rbulan = "Februari"
        break;
    case 3:
        rbulan = "Maret";
        break;
    case 4:
        rbulan = "April";
        break;
    case 5:
        rbulan = "Mei";
        break;
    case 6:
        rbulan = "Juni";
        break;
    case 7:
        rbulan = "Juli";
        break;
    case 8:
        rbulan = "Agustus";
        break;
    case 9:
        rbulan = "September";
        break;
    case 10:
        rbulan = "Oktober";
        break;
    case 11:
        rbulan = "November";
        break;
    case 12:
        rbulan = "Desember";
        break;
    default:
        fbulan = false;
        rbulan = `${bulan} tidak terdaftar sebagai bulan !`;
        break;
}

switch(true){
    case (tahun >= 1900 && tahun <= 2200):
        rtahun = tahun;
        break;
    default:
        ftahun = false;
        rtahun = `${tahun} tidak terdaftar sebagai tahun !`;
        break;
}


if (fhari == true && fbulan == true && ftahun == true){
    console.log(`${rhari} ${rbulan} ${rtahun}`);
} else {
    if (fhari == false){
        console.log(rhari);
    }

    if (fbulan == false){
        console.log(rbulan);
    }

    if (ftahun == false){
        console.log(rtahun);
    }
}