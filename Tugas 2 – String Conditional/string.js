// Soal No. 1 (Membuat kalimat)
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

var sentence1 = word.concat(" ", second, " ", third, " ", fourth, " ", fifth, " ", sixth, " ", seventh);
console.log(sentence); 
// Output: "JavaScript is awesome and I love it!"

// -------------------------------------------------------------
// Soal No.2 Mengurai kalimat (Akses karakter dalam string)
var sentence2 = "I am going to be React Native Developer"; 

var firstWord = sentence[0]; 
var secondWord = sentence[2] + sentence[3]; 
var thirdWord = sentence.substring(5, 10);
var fourthWord = sentence.substring(11, 13);
var fifthWord = sentence.substring(14, 16);
var sixthWord = sentence.substring(17, 22);
var seventhWord = sentence.substring(22, 29);
var eighthWord = sentence.substring(29, 39);

console.log('First Word: ' + firstWord);
// Output: I
console.log('Second Word: ' + secondWord);
// Output: am
console.log('Third Word: ' + thirdWord);
// Output: going
console.log('Fourth Word: ' + fourthWord);
// Output: to
console.log('Fifth Word: ' + fifthWord);
// Output: be
console.log('Sixth Word: ' + sixthWord);
// Output: React
console.log('Seventh Word: ' + seventhWord);
// Output: Native
console.log('Eighth Word: ' + eighthWord);
// Output: Developer

// -------------------------------------------------------------
// Soal No. 3 Mengurai Kalimat (Substring)
var sentence3 = 'wow JavaScript is so cool'; 

var firstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substring(4, 14);
var thirdWord3 = sentence3.substring(15,17);
var fourthWord3 = sentence3.substring(18,20);
var fifthWord3 = sentence3.substring(21, 25);

console.log('First Word: ' + firstWord3);
// Output: wow
console.log('Second Word: ' + secondWord3);
// Output: JavaScript
console.log('Third Word: ' + thirdWord3);
// Output: is
console.log('Fourth Word: ' + fourthWord3);
// Output: so
console.log('Fifth Word: ' + fifthWord3);
// Output: cool

// -------------------------------------------------------------
// Soal No. 4 Mengurai Kalimat dan Menentukan Panjang String

console.log('First Word: ' + firstWord3 + ', with length: ' + firstWord3.length);
// Output: First Word: wow, with length: 3
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWord3.length);
// Output: Second Word: JavaScript, with length: 10
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWord3.length);
// Output: is, with length: 2
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWord3.length);
// Output: so, with length: 2
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWord3.length);
// Output: cool, with length: 4