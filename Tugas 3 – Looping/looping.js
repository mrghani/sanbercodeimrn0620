// No. 1 Looping While
console.log("-------------------- No. 1 Looping While --------------------");

var flag = 2;
console.log("LOOPING PERTAMA");
while (flag <= 20) {
    console.log(`${flag} - I love coding`);
    flag += 2;
}

flag2 = 20
console.log("LOOPING KEDUA");
while (flag2 >= 2 ) {
    console.log(`${flag2} - I will become a mobile developer`);
    flag2 -= 2;
}


// ---------------------------------------------------------------
// No. 2 Looping menggunakan for
console.log("-------------------- No. 2 Looping menggunakan for ----------");

for (let i = 1; i <= 20; i++) {
    let status;
    if(i % 2 == 0){
        status = "Berkualitas";
    } else if(i % 3 == 0 && i % 2 == 1 ){
        status = "I Love Coding ";
    } else {
        status = "Santai";
    }
    console.log(`${i} - ${status}`);
}


// ---------------------------------------------------------------
// No. 3 Membuat Persegi Panjang
console.log("-------------------- No. 3 Membuat Persegi Panjang ----------");

let persegi = "";
for(let v = 1 ;v <= 4; v++ ){
    for(let h = 1; h <= 8; h++){
        persegi = persegi + "#"; 
    }

    persegi = persegi + " \n ";
    console.log(persegi);
    persegi = "" ;
}


// ---------------------------------------------------------------
// No. 4 Membuat Tangga
console.log("-------------------- No. 4 Membuat Tangga  ------------------");

let segitiga = "";
for (let v = 1; v <= 7; v++){
    for (let h = 1; h <= v; h++){
        segitiga = segitiga + "#";
    }
    segitiga = segitiga + " \n ";
    console.log(segitiga);
    segitiga = "";
}


// ---------------------------------------------------------------
// No. 5 Membuat Papan Catur
console.log("--------------------  No. 5 Membuat Papan Catur -------------");

let catur = "";
for (let v = 1; v <= 8; v++){
    if(v % 2 == 1){
        for (let h = 1; h <= 8; h++){
            if(h % 2 == 1){
                catur = catur + " ";
            } else {
                catur = catur + "#";
            }
        }
    } else {
        for (let h = 1; h <= 8; h++){
            if(h % 2 == 1){
                catur = catur + "#";
            } else {
                catur = catur + " ";
            }
        }
    }
    console.log(catur);
    catur = "";
}