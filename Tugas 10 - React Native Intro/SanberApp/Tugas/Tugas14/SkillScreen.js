import React, { Component } from 'react';
import { StyleSheet, Image, Text, View, TouchableOpacity, FlatList } from 'react-native';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      skillList: []  
    }
  }

  componentDidMount() {
    var data = fetch("./skillData.json")
      .then((res) => res.json())
      .then((resJson) => {
        this.state({
          dataSource: resJson.items,
          isLoading: false
        });
      }).catch((err) => {
        console.log(err);
      });
    return data;
  }

  render() {
    return (
      <View style={styles.container}>
          
          <Image style={styles.imgBrand} source={require("./images/logo.png")} />
          <View>
            <Image style={styles.imgUser} source={require("./images/user.png")} />
            <Text style={styles.imgText, {left: 90, top: 90, color: '#003366'}}>Hi,</Text>
            <Text style={styles.imgText, {left: 90, top: 93, fontWeight: 'bold', fontSize: 16}}>Mohamad Rizal Ghani</Text>
          </View>
          <Text style={styles.skillText} >Skills</Text>
          <View style={styles.line} />

        <View style={{top: 200, left: 17, paddingRight: 50}}>
          <TouchableOpacity style={{backgroundColor: '#B4E9FF', width: 140, height: 30, borderRadius: 10 }}>
            <Text style={styles.btnSkill}>Library/Framework</Text>
          </TouchableOpacity>
        </View>
        <View style={{top: 170, left: 160, paddingRight: 50}}>
          <TouchableOpacity style={{backgroundColor: '#B4E9FF', width: 105, height: 30, borderRadius: 10 }}>
            <Text style={styles.btnSkill}>Programming</Text>
          </TouchableOpacity>
        </View>
        <View style={{top: 140, left: 270, paddingRight: 50}}>
          <TouchableOpacity style={{backgroundColor: '#B4E9FF', width: 90, height: 30, borderRadius: 10 }}>
            <Text style={styles.btnSkill}>Technology</Text>
          </TouchableOpacity>
        </View>
        <FlatList
          data={this.state.dataSource}
          renderItem={(
            {item}) => <Text >{item.skillName}</Text>
          }
          keyExtractor={(item, index) => index}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  imgBrand: {
    position: 'absolute',
    width: 187.5,
    height: 51,
    left: 187,
    top: 25
  },
  imgUser: {
    position: 'absolute',
    width: 70,
    height: 70,
    left: 10,
    top: 80
  },
  imgText: {
    position: 'absolute',
    width: 200,
    height: 70
  },
  skillText: {
    position: 'absolute',
    width: 93,
    height: 42,
    left: 16,
    top: 180,
    fontSize: 36,
    lineHeight: 42,
    color: '#003366'
  },
  line: {
    width: 343,
    height: 5,
    left: 16,
    top: 185,
    backgroundColor: '#3EC6FF'
  },
  btnSkill: {
    left: 8,
    top: 5,
    alignContent: 'center',
    color: '#003366',
    fontWeight: 'bold'
  }
});
