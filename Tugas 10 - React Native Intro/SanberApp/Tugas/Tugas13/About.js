import React, { Component } from 'react';
import { ScrollView, StyleSheet, Image, Text, TextInput, View, TouchableOpacity } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

export default class App extends Component {
  render() {
    return (
        <View style={styles.container}>
          <Text style={styles.aboutText}>About Me</Text>
          <Image style={styles.imageStyle} source={require("./images/user.png")} />
          <Text style={styles.personText}>Mohamad Rizal Ghani</Text>
          <Text style={styles.positionText}>IT System Analyst</Text>

          <View style={styles.portofolioDiv} >
            <Text style={styles.AccountText}>Account</Text>
            <View style={{ top: 35,borderWidth: 1 }}></View>

            <FontAwesome style={styles.gitLabIcon} name="gitlab" size={35} color="orange" />
            <Text style={styles.gitLabAccount}>@Mohamad_Rizal_Ghani</Text>

            <FontAwesome style={styles.gitHubIcon} name="github" size={35} color="purple" />
            <Text style={styles.gitHubAccount}>@Rizal_Ghani</Text>
          </View>

        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageStyle: {
    position: 'absolute',
    width: 200,
    height: 200,
    left: 88,
    top: 118
  },
  aboutText: {
    position: 'absolute',
    width: 220,
    height: 42,
    left: 120,
    top: 64,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 36,
    lineHeight: 42,
    color: '#003366'
  },
  personText: {
    position: 'absolute',
    width: 303,
    height: 28,
    left: 97,
    top: 320,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 20,
    lineHeight: 28,
    color: '#003366'
  },
  positionText: {
    position: 'absolute',
    width: 168,
    height: 19,
    left: 125,
    top: 350,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 18,
    lineHeight: 19,
    color: '#3EC6FF'
  },
  portofolioDiv: {
    position: 'absolute', 
    width: 360, 
    height: 140, 
    left: 15, 
    top: 413, 
    backgroundColor: '#EFEFEF',
    borderRadius: 16
  },
  AccountText: {
    position: 'absolute',
    width: 90,
    height: 20,
    left: 16,
    top: 8,
    fontSize: 18,
    fontWeight: 'bold'
  },
  gitLabIcon: {
    position: 'absolute',
    left: '3.03%',
    right: '3.22%',
    top: '30%',
    bottom: '6.51%',
  },
  gitLabAccount: {
    position: 'absolute',
    left: '15%',
    right: '3.22%',
    top: '35%',
    bottom: '6.51%',
    fontSize: 16,
    color: 'orange'
  },
  gitHubIcon: {
    position:'absolute',
    left: '3.03%',
    right: '3.22%',
    top: '65%',
    bottom: '6.51%',
  },
  gitHubAccount: {
    position: 'absolute',
    left: '15%',
    right: '3.22%',
    top: '70%',
    bottom: '6.51%',
    fontSize: 16,
    color: 'purple'
  },
});
