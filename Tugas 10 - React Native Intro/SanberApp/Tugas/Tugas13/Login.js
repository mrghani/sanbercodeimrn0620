import React, { Component } from 'react';
import { ScrollView, StyleSheet, Image, Text, TextInput, View, TouchableOpacity } from 'react-native';


export default class App extends Component {
  render() {
    return (
        <View style={styles.container}>
          <Image style={styles.imageStyle} source={require("./images/logo.png")} />
          <Text style={styles.registerText}>Login</Text>

          <Text style={styles.usernameText}>Username</Text>
          <TextInput style={styles.usernameTextInput} />

          <Text style={styles.passwordText}>Password</Text>
          <TextInput style={styles.passwordTextInput} />

          <TouchableOpacity style={styles.registerButton} >
            <Text style={{ position: 'absolute', width: 90, height: 28, left: 38, marginTop: 5, color: '#FFFFFF', fontSize: 20 }} >Register</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.loginButton} >
            <Text style={{ position: 'absolute', width: 90, height: 28, left: 48, marginTop: 5, color: '#FFFFFF', fontSize: 20 }} >Login</Text>
          </TouchableOpacity>

        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageStyle: {
    position: 'absolute',
    width: 375,
    height: 102,
    left: 0,
    top: 63
  },
  registerText: {
    position: 'absolute',
    height: 28,
    left: 143,
    top: 200,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 24,
    lineHeight: 28,
    color: '#003366'
  },
  usernameText: {
    position: 'absolute',
    width: 73,
    height: 19,
    left: 40,
    top: 243,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 16,
    lineHeight: 19,
    color: '#003366'
  },
  usernameTextInput: {
    position: 'absolute',
    width: 294, 
    height: 48, 
    left: 40, 
    top: 266, 
    borderColor: '#003366', 
    borderWidth: 1 
  },
  passwordText: {
    position: 'absolute',
    width: 80,
    height: 19,
    left: 40,
    top: 330,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 16,
    lineHeight: 19,
    color: '#003366'
  },
  passwordTextInput: {
    position: 'absolute',
    width: 294, 
    height: 48, 
    left: 40, 
    top: 353, 
    borderColor: '#003366', 
    borderWidth: 1,
  },
  registerButton: {
    position: 'absolute',
    width: 140,
    height: 40,
    left: 117,
    top: 520,
    backgroundColor: '#003366',
    borderRadius: 16
  },
  loginButton: {
    position: 'absolute',
    width: 140,
    height: 40,
    left: 117,
    top: 450,
    backgroundColor: '#3EC6FF',
    borderRadius: 16
  },
});
