// Soal No. 1 (Range)
console.log("-------------------- No. 1 (Range) --------------------");

function range(num1, num2) {
    let arr = [];

    if(num1 != undefined && num2 != undefined){
        if (num1 < num2){
            for (let i = num1; i <= num2; i++){
                arr.push(i);
            }
        } else if (num1 > num2) {
            for (let i = num1; i >= num2; i--){
                arr.push(i);
            }
        }
    } else {
        arr.push(-1);
    }
    
    console.log(arr);
    arr = [];
}

range(1, 10);
range(1);
range(11, 18);
range(54, 50);
range();


// ---------------------------------------------------------------
// Soal No. 2 (Range with Step)
console.log("-------------------- No. 2 (Range with Step) --------------------");

function rangeWithStep (startNum, finishNum, step){
    let arr = [];

    if (startNum < finishNum){
        for (let i = startNum; i <= finishNum; i = i + step) {
            arr.push(i);
        }
    } else { //startNum < finishNum
        for (let i = startNum; i >= finishNum; i = i - step) {
            arr.push(i);
        }
    }
    
    console.log(arr);
    arr = [];
}

rangeWithStep(1, 10, 2);
rangeWithStep(11, 23, 3);
rangeWithStep(5, 2, 1);
rangeWithStep(29, 2, 4);

// ---------------------------------------------------------------
// Soal No. 3 (Sum of Range)
console.log("-------------------- No. 3 (Sum of Range) --------------------");

function sum(startNum, finishNum, step) {
    let arr = [];

    if (startNum != undefined && finishNum != undefined){
        step = step != undefined ? step : 1;

        if (startNum < finishNum){
            for (let i = startNum; i <= finishNum; i = i + step) {
                arr.push(i);
            }
        } else { //startNum > finishNum
            for (let i = startNum; i >= finishNum; i = i - step) {
                arr.push(i);
            }
        }        
    } else {
        if (startNum != undefined || finishNum != undefined){
            arr.push(1);
        } else {
            arr.push(0);
        }
    }

    console.log(arr.reduce((a, b) => a + b));
    arr = [];
}

sum(1,10); // 55
sum(5, 50, 2); // 621
sum(15,10); // 75
sum(20, 10, 2); // 90
sum(1); // 1
sum(); // 0 

// ---------------------------------------------------------------
// Soal No. 4 (Array Multidimensi)
console.log("-------------------- No. 4 (Array Multidimensi) --------------------");

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling(input) {
    let profile;
    input.forEach(x => {
        profile = `Nomor ID: ${x[0]}\nNama Lengkap: ${x[1]}\nTTL: ${x[2]} ${x[3]}\nHobi: ${x[4]}\n`;
        profile = profile + "-------------------------------";

        console.log(profile);
        profile = "";
    });
}

dataHandling(input);


// ---------------------------------------------------------------
// Soal No. 5 (Balik Kata)
console.log("-------------------- No. 5 (Balik Kata) --------------------");

function balikKata(str) {
    return str.split('').reverse().join('');
}

console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I 

// ---------------------------------------------------------------
// Soal No. 6 (Metode Array)
console.log("-------------------- No. 6 (Metode Array) --------------------");

function dataHandling2(input) {
    // (6 - A)
    input.splice(1, 1, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
    input.splice(2, 2, "Provinsi Bandar Lampung");
    input.splice(4, 4, "Pria", "SMA Internasional Metro");

    console.log(input);

    // (6 - B)
    let _date = input[3];
    let _month = parseInt(_date.slice(3,5));
    let _result;

    switch (_month) {
        case 1:
            _result = "Januari";
            break;
        case 2:
            _result = "Februari";
            break;
        case 3:
            _result = "Maret";
            break;
        case 4:
            _result = "April";
        break;
        case 5:
            _result = "Mei";
        break;
        case 6:
            _result = "Juni";
        break;
        case 7:
            _result = "Juli";
        break;
        case 8:
            _result = "Agustus";
        break;
        case 9:
            _result = "September";
        break;
        case 10:
            _result = "Oktober";
        break;
        case 11:
            _result = "November";
        break;
        case 11:
            _result = "Desember";
        break;
        default:
            _result = `${_month} tidak dikenali !`;
        break;
    }

    console.log(_result);

    // (6 - C)
    let _date3 = input[3].split("/").reverse();

    function move(_input, from, to) {
        let numberOfDeletedElm = 1;
      
        const elm = _date3.splice(from, numberOfDeletedElm)[0];
      
        numberOfDeletedElm = 0;
      
        _date3.splice(to, numberOfDeletedElm, elm);

        return _date3;
    }

    console.log(move(_date3, 1, 2));
    // Output: [ '1989', '21', '05' ]

    // (6 - D)
    let _date4 = _date.split("/").join("-");
    console.log(_date4);

    // (6 - E)
    let _name = input[1].slice(0, 15);
    console.log(`Name: ${_name}`);

    input = [];
}



var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
