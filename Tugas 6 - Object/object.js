// Soal No. 1 (Array to Object)

function arrayToObject(arr) {
    var personList = [];

    if (arr == undefined){
        return "empty value ...";
    } else {
        for (let i = 0; i < arr.length; i++) {
            let _obj = arr[i];

            let _fullName = `${_obj[0]} ${_obj[1]}`;
            let _firstName = _obj[0];
            let _lastName = _obj[1];
            let _gender = _obj[2];

            let _age;
            if (_obj[3] != undefined){
                let now = new Date();
                let year = now.getFullYear();

                if (_obj[3] < year){
                    _age = now.getFullYear() - _obj[3];
                } else {
                    _age = "Invalid birth year"
                }
            } else {
                _age = "Invalid birth year";
            }

            let person = {
                fullName: _fullName,
                detail: {
                    firstName: _firstName,
                    lastName: _lastName,
                    gender: _gender,
                    age: _age
                }
            };
            personList.push(person);
            person = {};
        }
        return personList;
    }
}

var people1 = [ 
    ["Bruce", "Banner", "male", 1975], 
    ["Natasha", "Romanoff", "female"] 
]

var people2 = [ 
    ["Tony", "Stark", "male", 1980], 
    ["Pepper", "Pots", "female", 2023]
]

// ---------------------------- Soal No. 1 (Array to Object) ----------------------------
console.log(arrayToObject(people1));
console.log(arrayToObject(people2));
console.log(arrayToObject());



// Soal No. 2 (Shopping Time)

function shoppingTime(memberId, money) {
    let result = {
        memberId : memberId,
        money : money,
        listPurchased: [],
        changeMoney : 0
    }

    if (memberId == "" || memberId == undefined){
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    } else {
        if (money <= 50000 ){
            return "Mohon maaf, uang tidak cukup";
        } else {
            let listitem = [
                { item: "Sepatu", brand: "Stacattu", price: 1500000},
                { item: "Baju", brand: "Zoro", price: 500000},
                { item: "Baju", brand: "H&N", price: 250000},
                { item: "Sweater", brand: "H&Uniklooh", price: 175000},
                { item: "Casing Handphone", brand: "", price: 50000}
            ]

            let buyItems = [];
            let moneyNow = 700000;

            listitem.forEach(x => {
                if (moneyNow > x.price){
                    let _product = `${x.item} ${x.brand}`;
                    buyItems.push(_product);
                    moneyNow = moneyNow - x.price;
                }
            });
            
            result.listPurchased = buyItems;
            result.changeMoney = moneyNow;
        }
        return result;
    }
}

// ---------------------------- Soal No. 2 (Shopping Time) ------------------------------
console.log(shoppingTime('324193hDew2', 700000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


// Soal No. 3 (Naik Angkot)

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var angkot = [{},{}];
    var i = 0;
    var from = ""
    var to = "";
  
    for (i; i<arrPenumpang.length; i++) {

        for (j = 0; j < arrPenumpang[i].length; j++) {
            switch (j) {
                case 0: {
                    angkot[i].penumpang = arrPenumpang[i][j];
                    break;
                } case 1: {
                    angkot[i].naikDari = arrPenumpang[i][j];
                    angkot[i].to = arrPenumpang[i][j+1];
                    break;
                } case 2: {
                    from = arrPenumpang[i][j-1];
                    to = arrPenumpang[i][j];
                    var jarak = 0;
                    for (var k=0; k < rute.length; k++) {
                        if (rute[k] === from) {
                            for (var l = k + 1; l < rute.length; l++) {
                                jarak += 1;
                                if (rute[l] === to) {
                                    var bayar = jarak * 2000;
                                    angkot[i].bayar = bayar;
                                }
                            }
                        }
                    }
                    break;
                }
            }
        }
    }
  return angkot;
  }
  
// ---------------------------- Soal No. 3 (Naik Angkot) --------------------------------
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
  
  // console.log(naikAngkot([])); //[]
  